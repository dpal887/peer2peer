__author__ = 'ubuntu'

import sqlite3

import Common

"""
The following is pretty self explanitory. Functions to get, set, clear and update items in the database
"""

try:
    # Clear ip list temporary list e.c.t. on exit / engine stop
    def cleanup_database():
        with sqlite3.connect(Common.AD_STRING) as con:
            con.execute("DELETE FROM ip_string")
            con.execute("DELETE FROM info_string")
            con.execute("DELETE FROM user_temp")


    def clear_list():
        with sqlite3.connect(Common.AD_STRING) as con:
            con.execute("DELETE FROM ip_string")
        con.close()


    def add_group_string(groupID, user_list):
        with sqlite3.connect(Common.AD_STRING) as con:
            con.execute("DELETE FROM user_temp")
        con.close()

        with sqlite3.connect(Common.DB_STRING) as c:
            o = c.execute("INSERT INTO group_string VALUES (?, ?)",
                          [groupID, user_list])
        o.close()


    def get_from_group(groupID):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT userList FROM group_string WHERE groupID=?", (groupID,))
        mesg = output.fetchall()
        return mesg


    def get_group_id(user_list):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT groupID FROM group_string WHERE userList=?", [user_list])
        mesg = output.fetchall()
        return mesg


    def get_message_from_group(groupID):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT message FROM user_string WHERE groupID=?", [groupID])
        mesg = output.fetchall()
        return mesg


    def get_all_user_from_group():
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT userList FROM group_string")
        return output.fetchall()


    def get_all_id_from_group():
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT groupID FROM group_string")
        return output.fetchall()


    def update_table(sender, destination, message, groupID, clean=True):
        if clean:
            message = Common.clean(message)

        with sqlite3.connect(Common.DB_STRING) as c:
            c.execute("INSERT INTO user_string VALUES (?, ?, ?, ?)",
                      [sender, destination, message, groupID])
        c.close()


    def all_ip():
        with sqlite3.connect(Common.AD_STRING) as c:
            con = c.execute(
                "SELECT username, location, ip, port, last_login, publicKey FROM ip_string ORDER BY last_login DESC")
        mesg = con.fetchall()
        con.close()
        return mesg


    def update_ip(local, username, location, ip, port, last_login, pubkey):
        local = local.replace(' ', '')
        username = username.replace(' ', '')
        location = location.replace(' ', '')
        ip = ip.replace(' ', '')
        port = str(port).replace(' ', '')
        last_login = str(last_login).replace(' ', '')
        pubkey = str(pubkey).replace(' ', '')
        with sqlite3.connect(Common.AD_STRING) as c:
            c.execute(
                "INSERT INTO ip_string(local_id, username, location, ip, port, last_login, publicKey) VALUES (?, ?, ?, ?, ?, ?, ?)",
                [local, username, location, ip, port, last_login, pubkey])
        c.close()


    def update_alert(alert, username):
        with sqlite3.connect(Common.AD_STRING) as c:
            c.execute("UPDATE ip_string SET alert = ? WHERE username = ?",
                      [alert, username])
        c.close()


    def pull_alert(username):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT alert FROM ip_string WHERE username=?", [username])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_message(search_term):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT message FROM user_string")
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_user(search_term):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT sender FROM user_string")
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_user(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT username FROM ip_string ORDER BY last_login DESC")
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_ip_srch(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT ip FROM ip_string WHERE username=? ORDER BY last_login DESC", [search_term, ])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_user_srch(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT username FROM ip_string WHERE ip=? ORDER BY last_login DESC", [search_term, ])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_user_id(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT username FROM ip_string WHERE ROWID=? ORDER BY last_login DESC", [search_term, ])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_sender_user_id(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT username FROM ip_string WHERE ROWID=? ORDER BY last_login DESC", [search_term, ])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_id(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT ROWID FROM ip_string WHERE ip=? ORDER BY last_login DESC", [search_term, ])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_message_id(search_term, search_term2):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT sender,message FROM user_string WHERE destination=? OR sender=? ORDER BY ROWID DESC",
                               [search_term, search_term2])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_sender_id(message):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT sender FROM user_string WHERE message = ?",
                               [message])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_destination_id(message):
        with sqlite3.connect(Common.DB_STRING) as c:
            output = c.execute("SELECT destination FROM user_string WHERE message = ?",
                               [message])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_location(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT location FROM ip_string ORDER BY last_login DESC")
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_ip(search_term):
        try:
            with sqlite3.connect(Common.AD_STRING) as c:
                output = c.execute("SELECT ip FROM ip_string ORDER BY last_login DESC")
                mesg = output.fetchall()
                output.close()
        except sqlite3.DatabaseError:
            mesg = 'Failed'
        return mesg


    def pull_peer_port(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT port FROM ip_string ORDER BY last_login DESC")
        mesg = output.fetchall()
        output.close()

        return mesg


    def get_port_from_username(username):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT port FROM ip_string WHERE username=? ORDER BY last_login DESC", [username, ])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_peer_ll(search_term):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT last_login FROM ip_string ORDER BY last_login DESC")
        mesg = output.fetchall()
        output.close()
        return mesg


    def update_ping(username, ping):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("UPDATE ip_string SET ping = ? WHERE username = ?", [ping, 'dpal887'])
        mesg = output.fetchall()
        output.close()
        return mesg


    def pull_ping(username):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT ping FROM info_string WHERE username = ?", [username])
        mesg = output.fetchone()
        output.close()
        return mesg


    def update_info_table(username, ping, apilist):
        with sqlite3.connect(Common.AD_STRING) as c:
            c.execute("DELETE FROM info_string WHERE username = ?", [username])
            c.execute("INSERT INTO info_string VALUES (?, ?, ?)",
                      [username, ping, str(apilist)])
        c.close()


    def update_user_list(ll, username):
        with sqlite3.connect(Common.AD_STRING) as c:
            c.execute("UPDATE ip_string SET last_login = ? WHERE username = ?",
                      [ll, username])
        c.close()


    def pull_peer_key(username):
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT publicKey FROM ip_string WHERE username = ? ORDER BY last_login DESC",
                               [username, ])
        mesg = output.fetchall()[0][0]
        output.close()
        return mesg


    def delete_user(username):
        with sqlite3.connect(Common.AD_STRING) as c:
            c.execute("DELETE FROM info_string WHERE username = ?", [username])
            c.execute("DELETE FROM ip_string WHERE username = ?", [username])
        c.close()


    def add_group_member(username):
        with sqlite3.connect(Common.AD_STRING) as c:
            c.execute("INSERT INTO user_temp VALUES (?)", [username])
        c.close()


    def pull_group_members():
        with sqlite3.connect(Common.AD_STRING) as c:
            output = c.execute("SELECT username FROM user_temp")
        mesg = output.fetchall()
        c.close()
        return mesg
except:
    print('Database Error')


