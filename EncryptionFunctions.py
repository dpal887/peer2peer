import hashlib
import binascii
from Crypto.Cipher import AES
from Crypto.PublicKey import RSA
import os

import Common


BLOCK_SIZE = 16

__author__ = 'ubuntu'

key = ''


def xor_enc(message):
    xorKey = 105
    message = [ord(character) for character in message]
    listing = []
    for i in message:
        listing.append(chr(i ^ xorKey))
    return "".join(listing)


def RSA_ENCR(pkey, unenc):
    unenc = unenc.encode('utf-8')
    pkey = pkey.decode('hex')
    pkey = RSA.importKey(pkey)
    enc = pkey.encrypt(unenc, '')
    enc = enc[0].encode('hex')

    return enc


def RSA_DCR(enc):
    enc = enc.decode('hex')
    unenc = key.decrypt(enc)
    unenc = unenc.decode('utf-8')

    return unenc


def keyRSA():
    global key
    key = RSA.generate(1024)
    pubkey = key.publickey()
    pubkey = pubkey.exportKey(format='DER')
    pubkey = pubkey.encode('hex')
    return pubkey


def hash256(unhsh):
    unhsh += 'COMPSYS302-2015'
    hsh = hashlib.sha256(unhsh).hexdigest()
    return hsh


def aes256_enc(un_enc, id='server'):
    while (len(un_enc) % 16) != 0:
        un_enc += " "
    iv = os.urandom(BLOCK_SIZE)
    if id == 'server':
        enc = AES.new(Common.server_key, AES.MODE_CBC, iv)
    elif id == 'message':
        enc = AES.new(Common.api_key, AES.MODE_CBC, iv)
    else:
        enc = AES.new(Common.server_key, AES.MODE_CBC, iv)
    encrypted_message = iv + enc.encrypt(un_enc)
    return encrypted_message.encode('hex')


# unit test
def aes_decrypt(message, id='server'):
    enc = binascii.unhexlify(message)
    iv = enc[:16]
    if id == 'message':
        cipher = AES.new(Common.api_key, AES.MODE_CBC, iv)
    else:
        cipher = AES.new(Common.server_key, AES.MODE_CBC, iv)

    return cipher.decrypt(enc[16:]).rstrip(' ')


PUBLIC_KEY = keyRSA()
e = aes256_enc('dpal887', 'message')


'''-----------------------------------------------------------------------------------------'''
'''                                  HASHING                                                '''
'''-----------------------------------------------------------------------------------------'''
'''md5'''


def md5(mesg):
    enc_mesg = hashlib.md5(mesg).hexdigest()
    return enc_mesg


'''md5 - Salt'''


def md5_salt(mesg, username):
    mesg += username
    enc_mesg = hashlib.md5(mesg).hexdigest()
    return enc_mesg


'''SHA-256'''


def SHA_256(mesg):
    enc_mesg = hashlib.sha256(mesg).hexdigest()
    return enc_mesg


'''SHA-256 - Salt'''


def SHA_256_salt(mesg, username):
    mesg += username
    enc_mesg = hashlib.sha256(mesg).hexdigest()
    return enc_mesg


'''SHA-512'''


def SHA_512(mesg):
    enc_mesg = hashlib.sha512(mesg).hexdigest()
    return enc_mesg


'''SHA-512 - Salt'''


def SHA_512_salt(mesg, username):
    mesg += username
    enc_mesg = hashlib.sha512(mesg).hexdigest()
    return enc_mesg

