import thread
import time

import EncryptionFunctions


__author__ = 'ubuntu'

import cherrypy
import Common
import db_mgr
import os
import Version1
import ServerSide

# TODO: User can delete messages

# Start the reporting thread
thread.start_new_thread(Version1.checkin,
                        (Common.location,))

'''The global config for cherrypy session'''

_cp_config = {'tools.encode.on': True,
              'tools.encode.encoding': 'utf-8',
              'tools.sessions.on': 'True',
              }

'''Holds the current person you are talking to ip'''
PEER_IP = Common.myIP + ':10003'
PEER_USERNAME = 'dpal887'

'''Flag if the peer selected is a group'''
IS_GROUP = False

'''Global name for the thread'''
REPORTING_THREAD = thread

'''Global variable to show message alerts'''
MESSAGE_ALERT = ''

'''Global variable to change bg color'''
COLOR = 'red'

'''Gets the current users api list'''
APILIST = ['noApi']


class Main(object):
    """
    This is the main page: Simply directs the user to login.
    Can also be accessed using the top bar p2py button
    """

    @cherrypy.expose()
    def index(self):
        page = file('index.html', 'r').read()
        page += '<div class="container">'
        page += '<div class="jumbotron">'
        page += '<h1 align="center">Welcome to Python P2P Chat</h1>'
        page += '<p align="center">Created By Daniel Paley</p>'
        page += '<div align="center"><p align="center">'
        page += '<form action="/login">'
        page += '<input class="btn btn-lg btn-success" type="submit" value="Login">'
        page += '</form>'
        page += '</div>'
        page += '</div>'
        page += '</div>'
        page += file('EndIndex.html', 'r').read()
        return page

    """
    This is the login screen. Two levels of encryption are supported
    Hash and AES
    Argument error helps the function to print any error message returned from the server
    """

    @cherrypy.expose
    def login(self, error='', location='0'):
        Common.location = location
        if location == '1':
            Common.myIP = Common.getNetworkIp()
        else:
            Common.myIP = Common.getPublicIP()
        # Common initial page initialisation file
        page = file('index.html', 'r').read()
        page += '<div class="container"><div class="jumbotron">'
        page += '<h3 align="center"'
        # Change color of text based on encryption type
        if Version1.encryption == 'AES':
            page += ' style="color:green"'
        else:
            page += ' style="color:red"'
        page += ' align="center">YOUR LOGIN IS ENCRYPTED WITH ' + Version1.encryption + '-256 </h2>'
        # Add sign in static html form
        page += file('SignIn.html', 'r').read()
        page += '<h2 align="center"> Your location is set to:'
        if location == '0':
            page += 'University Desktop'
        if location == '1':
            page += 'University Wireless'
        if location == '2':
            page += 'Rest of the world'
        page += '</h2>'
        # Print error if any
        if error != '':
            page += '<font color=red><h1 align="center">' + error + '</h1></font>'

        """
        If the server error was a url error then the connection to server failed.
        Implement fallback system on button click
        """

        if error == Common.Emesg('11'):
            page += '<div align=center>'
            page += '<a href="/fallback"><button class="btn btn-warning btn-xl">Fallback System</button></a>'
            page += '</div>'
            page += '</div>'
            page += '</div>'

        # End the file html
        page += file('EndIndex.html', 'r').read()
        return page

    """
    Page is shown when fallback is implemented. Enter ip address and redirect to proper get list
    """

    @cherrypy.expose()
    def fallback(self):
        Version1.reporting_override = True
        page = file('index.html', 'r').read()
        page += '<form action="/setFallbackPeer">'
        page += '<input type="text" name="address"></input>'
        page += '<button type="submit">Submit</button>'
        page += '</form>'
        page += '<h1>OR</h1>'
        # Clicking this button starts the offline network. I.e. the first person to implement fallback.
        page += '<a href="/setStartingPeer"><button>Click to start the chain</button></a>'
        page += file('EndIndex.html', 'r').read()
        return page

    @cherrypy.expose()
    def setStartingPeer(self):
        Version1.pingOnly = True
        Version1.killthread = True
        Version1.wait = False
        # Set the person you want to connect to
        db_mgr.update_ip('', cherrypy.session['username'], Common.location, Common.myIP, Common.myPort,
                         int(time.time()), EncryptionFunctions.PUBLIC_KEY)
        # TODO: ADD A START CONNECTION BUTTON AND ADD OWN IP ADDRESS TO LIST
        raise cherrypy.HTTPRedirect('/client')

    """
    Sets thread to stop as we do not want to be constantly updating our list, (others update to us)
    """

    @cherrypy.expose()
    def setFallbackPeer(self, address):
        Version1.killthread = True
        Version1.wait = False
        # Set the person you want to connect to
        ServerSide.fallback_address = address
        # TODO: ADD A START CONNECTION BUTTON AND ADD OWN IP ADDRESS TO LIST
        raise cherrypy.HTTPRedirect('/client')

    """
    This is where the password and username is checked. Error code is returned from the server. If successful the user
    will be directed to the main page
    """

    @cherrypy.expose()
    def signin(self, username=None, password=None):
        username = Common.clean(username)
        password = Common.clean(password)
        code = self.authenitcate(username, password)
        # Check the code returned
        code_int = int(code.split(',')[0])
        if code_int == 0:
            # If success set username and password fields
            cherrypy.session['username'] = username
            cherrypy.session['password'] = password
            Version1.user = username
            Version1.password = password

            Version1.killthread = True
            Version1.wait = False

            # Redirect to the login server
            raise cherrypy.HTTPRedirect('/client')
        elif code_int < 8:
            # If the error is regarding a login problem return the error to the login page
            raise cherrypy.HTTPRedirect('/login?error=' + code)
        else:
            # If the error is regarding a server connection issue set the username and password to avoid errors
            cherrypy.session['username'] = username
            cherrypy.session['password'] = password
            # Redirect to the login page with error that will give fallback option
            raise cherrypy.HTTPRedirect('/login?error=' + code)


    @cherrypy.expose()
    def authenitcate(self, username=None, password=None):
        # Check returned value from server attempt
        code = Version1.report(username, password, Common.location)
        return code

    '''
    This is the client function. It runs the main chat system
    '''


    @cherrypy.expose()
    def client(self, response='0'):
        global APILIST
        global PEER_IP

        page = file('index.html', 'r').read()
        page += '<link href="/static/css/Carousel.css" rel="stylesheet">'
        page += '<style>body {background: radial-gradient(' + COLOR + ',black)}</style>'

        page += '<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>'
        page += '<script>'
        page += '   function autoRefresh_div(){'
        page += '   $("#divMesg").load("test");'
        page += '   $("#divIP").load("testtwo");'
        page += '   }'
        page += "   setInterval('autoRefresh_div()', 1000);"
        page += '</script>'

        page += '<div class="container">'
        page += '<div class="jumbotron">'
        page += '   <h1>'
        try:
            page += '   Welcome: ' + cherrypy.session['username']
        except KeyError:
            raise cherrypy.HTTPRedirect('/login?error=' + Common.Emesg('2'))
        page += '   </h1>'
        page += '   <div class="row">'
        page += '       <div class="col-xs-8">'
        page += '           <h2>You are talking to: ' + PEER_USERNAME + '</h2>'
        page += '       </div>'
        page += '       <div class="col-xs-4">'
        page += '           <h2>Encryption-' + ServerSide.encryption + '<span style="color: ' + COLOR + '" class="glyphicon glyphicon-lock pull-right" aria-hidden="true"></span></h2>'
        page += '       </div>'
        page += '   </div>'
        page += '   <div class="row">'
        page += '       <div class="col-xs-8">'
        page += '           <div class="scrollable">'

        page += '<div class="panel-body list-group-item-info square">'
        if not IS_GROUP:
            page += PEER_USERNAME + 'supports:<br>'
        else:
            page += PEER_IP + 'supports:<br>'

        # I could use this in the report ->  Add api list for groups
        # The following sets the icons for the api-ist:-----------------------------------------------------------
        Security_Num = 1

        for j in range(1, len(APILIST)):
            if 'Encoding' in APILIST[j]:
                Security_Num = j
                break
            page += '<span class="badge">' + Common.clean(APILIST[j]) + '</span>'

        page += '<br>'

        Security = []
        for k in range(Security_Num, len(APILIST) - 1):
            Security.append(APILIST[k].split(' '))
        for Security_type in Security:
            if Security_type[0].lower() == 'encoding':
                for number in range(0, len(Security_type)):
                    if number == 0:
                        page += '<br><span class="badge"> Encoding: </span>'
                    if number > 0:
                        page += Security_type[number]
                        try:
                            if int(Security_type[number]) == 0:
                                page += '<span class="pull-right glyphicon glyphicon-font" aria-hidden="true"></span>'
                            if int(Security_type[number]) > 0:
                                page += '<span class="pull-right glyphicon glyphicon-globe" aria-hidden="true"></span>'
                        except ValueError:
                            page += ''
            if Security_type[0].lower() == 'encryption':
                for number in range(0, len(Security_type)):
                    if number == 0:
                        page += '<br><span class="badge"> Encryption: </span>'
                    if number > 0:
                        page += Security_type[number]
                        try:
                            if int(Security_type[number]) == 0:
                                page += '<span style="color: red" class="pull-right glyphicon glyphicon-remove" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 1:
                                page += '<span style="color: grey" class="pull-right glyphicon glyphicon-lock" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 2:
                                page += '<span style="color: yellow" class="pull-right glyphicon glyphicon-lock" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 3:
                                page += '<span style="color: green" class="pull-right glyphicon glyphicon-lock" aria-hidden="true"></span>'
                        except ValueError:
                            page += ''
            if Security_type[0].lower() == 'hashing':
                for number in range(0, len(Security_type)):
                    if number == 0:
                        page += '<br><span class="badge"> Hashing: </span>'
                    if number > 0:
                        try:
                            page += Security_type[number]
                            if int(Security_type[number]) == 0:
                                page += '<span  style="color: red" class="pull-right glyphicon glyphicon-remove" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 1:
                                page += '<span style="color: orangered" class="pull-right glyphicon glyphicon-qrcode" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 2:
                                page += '<span style="color: orange" class="pull-right glyphicon glyphicon-qrcode" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 3:
                                page += '<span style="color: yellow" class="pull-right glyphicon glyphicon-qrcode" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 4:
                                page += '<span style="color: greenyellow" class="pull-right  glyphicon glyphicon-qrcode" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 5:
                                page += '<span style="color: green" class="pull-right glyphicon glyphicon-qrcode" aria-hidden="true"></span>'
                            if int(Security_type[number]) == 6:
                                page += '<span style="color: cyan" class="pull-right glyphicon glyphicon-qrcode" aria-hidden="true"></span>'
                        except ValueError:
                            page += ''
                            # End of list of icons for api

        page += '</div>'

        page += '               <div id="divMesg"></div>'
        page += '           </div>'
        page += '<div class="input-group">'
        page += '<form action="/post" method="post" enctype="multipart/form-data">'
        page += '<label for="inputUsername" class="sr-only">Message:</label>'
        page += '<span class="input-group-btn">'
        page += '<input width=500px id="inputUsername" name="message" class="form-control" placeholder="Message" required="True" autofocus="" type="text">'
        page += '<button name="ip" class="btn btn-info bloc" type="submit" required="True" value="' + PEER_IP + '">Send</button></form>'
        page += '</span>'
        page += '</div>'

        try:
            response = int(response)
        except TypeError:
            response = 12

        # Output a message based on what happened to the message
        # 500 and refusal errors shown in red
        if response > 9:
            page += '<div class="alert alert-danger" role="alert">' + Common.Emesg(str(response)) + '</div>'
        # Receiver side refusal errors
        elif response > 0:
            page += '<div class="alert alert-warning" role="alert">' + Common.Emesg(str(response)) + '</div>'
        # Success message
        elif response == 0:
            page += '<div class="alert alert-success" role="alert">' + Common.Emesg(str(response)) + '</div>'
        else:
            page += '<div class="alert alert-info" role="alert">Send a Message</div>'

        page += '        </div>'
        page += '        <div class="col-xs-4 scrollable">'
        page += '           <div id="divIP"></div>'
        page += '        </div>'
        page += '    </div>'
        page += '    </div>'
        page += '</div>'

        page += file('EndIndex.html', 'r').read()
        return page

    @cherrypy.expose()
    def test(self):
        global PEER_USERNAME
        global PEER_IP
        #Check if messages from users or from group
        if not IS_GROUP:
            messages_from_peer = db_mgr.pull_peer_message_id(PEER_USERNAME, PEER_USERNAME)
            page = '<ul class="list-group">'
            for message in messages_from_peer:
                page += '<li class="list-group-item"><strong>' + Common.clean(message[0]) + ': </strong>' + Common.clean(message[1]) + '</li>'
            page += '</ul>'
        else:
            messages_from_peer = db_mgr.get_message_from_group(PEER_USERNAME)
            page = '<ul class="list-group">'
            for message in messages_from_peer:
                page += '<li class="list-group-item"><strong>' + Common.clean(db_mgr.pull_peer_sender_id(str(message[0]))[0][0]) + ': </strong>' + Common.clean(str(message[0])) + '</li>'
            page += '</ul>'



        return page

    @cherrypy.expose()
    def testtwo(self, group='False'):
        page = ''
        # Initial for loop for the single users online (not grouped)
        # Different Colors and icons are displayed based on the user status. Read the help menu at /help which
        # explains these icons.
        for i in range(0, len(db_mgr.pull_peer_ip(None))):
            page += '<div class="list-group">'

            # If not in the group chat menu then set the action to set the ip address for sending the message
            # Otherwise add the user to the temporary group list in group chat. A more detailed explanation can be found
            # at /help

            if group == 'False':
                page += '<a href="/set_peer?ip=' + db_mgr.pull_peer_ip(None)[i][0] + ':' + \
                        db_mgr.pull_peer_port(None)[i][0] + '&username=' + db_mgr.pull_peer_user(None)[i][
                            0] + '" class="list-group-item">User: <font color='
            else:
                page += '<a href="/addUser?username=' + db_mgr.pull_peer_user(None)[i][
                    0] + '" class="list-group-item">User: <font color='
            # Set the font color based on peer location
            if db_mgr.pull_peer_location(None)[i][0] == Common.location:
                page += 'green'
            else:
                page += 'red'
            page += '>' + db_mgr.pull_peer_user(None)[i][0]
            # Set icons based on status
            if db_mgr.pull_alert(db_mgr.pull_peer_user(None)[i][0])[0][0] == '1':
                page += '&nbsp&nbsp&nbsp&nbsp<span style="color: orange"class="glyphicon glyphicon-envelope" aria-hidden="true"></span>'
            if db_mgr.pull_peer_location(None)[i][0] == '0':
                page += '<span class="glyphicon glyphicon-briefcase pull-right" aria-hidden="true"></span>'
            elif db_mgr.pull_peer_location(None)[i][0] == '1':
                page += '<span class="glyphicon glyphicon-education pull-right" aria-hidden="true"></span>'
            else:
                page += '<span class="glyphicon glyphicon-home pull-right" aria-hidden="true"></span>'

            page += '</font></a>'
            if int((time.time() - float(db_mgr.pull_peer_ll(None)[i][0]))) < 60:
                page += '<li href="#" class="list-group-item list-group-item-success">Seen: '
            elif int((time.time() - float(db_mgr.pull_peer_ll(None)[i][0]))) < 120:
                page += '<li href="#" class="list-group-item list-group-item-info">Seen: '
            elif int((time.time() - float(db_mgr.pull_peer_ll(None)[i][0]))) < 300:
                page += '<li href="#" class="list-group-item list-group-item-warning">Seen: '
            else:
                page += '<li href="#" class="list-group-item list-group-item-danger">Seen: '
            page += str(int(time.time() - float(db_mgr.pull_peer_ll(None)[i][0])) / 60) + " Minutes and " + str(
                int(time.time() - float(db_mgr.pull_peer_ll(None)[i][0])) % 60) + ' Seconds ago'
            try:
                if db_mgr.pull_ping(db_mgr.pull_peer_user(None)[i][0])[0][0] == '0':
                    page += '<span style="color: green" class="glyphicon glyphicon-signal pull-right" ' \
                            'aria-hidden="true"></span>'
                elif db_mgr.pull_ping(db_mgr.pull_peer_user(None)[i][0])[0][0] == '2':
                    page += '<span style="color: red" class="glyphicon glyphicon-search pull-right" ' \
                            'aria-hidden="true"></span>'
                else:
                    page += '<span style="color: blue" class="glyphicon glyphicon-remove pull-right" ' \
                            'aria-hidden="true"></span>'
            except IndexError:
                page += '<span style="color: orange" class="glyphicon glyphicon-refresh pull-right" ' \
                        'aria-hidden="true"></span>'
            except TypeError:
                page += '<span style="color: orange" class="glyphicon glyphicon-refresh pull-right" ' \
                        'aria-hidden="true"></span>'

            if group == 'False':
                page += '<a href="/set_sender_ip" name="ip" value="' + db_mgr.pull_peer_ip(None)[i][0] + ':' + \
                        db_mgr.pull_peer_port(None)[i][0] + '">'
            else:
                page += '<a href="/addUser" name="username" value="' + db_mgr.pull_peer_user(None)[i][0] + '">'

            page += '</a>'
            page += '</div>'
        # The following shows the group list under the user list
        groups = db_mgr.get_all_user_from_group()
        getid = db_mgr.get_all_id_from_group()
        for j in range(0, len(getid)):
            page += '<ul class="list-group">'
            # This time list the peer_ip is set to be the group name
            page += '<a href="set_peer?ip=' + groups[j][0] + '&username=' + getid[j][0] + '" class="list-group-item">' + getid[j][0] + '</a>'
            page += '<li class="list-group-item">' + groups[j][0] + '</li>'
            page += '</ul>'

        return page


    @cherrypy.expose()
    def set_peer(self, ip, username):
        global PEER_IP
        global PEER_USERNAME
        global IS_GROUP
        global APILIST

        PEER_IP = ip
        PEER_USERNAME = username

        db_mgr.update_alert('0', username)
        try:
            # Do this to determine if its a group or something else. Not great but it works
            ip.split('.')[2]
            IS_GROUP = False
            # Grab the users api list once
            APILIST = Version1.peerAPI(ip)
        except:
            # If its not an ip address its a group id
            IS_GROUP = True
        raise cherrypy.HTTPRedirect('/client')

    """
    The following is setting the senders ip address based on which user has been clicked
    """

    @cherrypy.expose()
    def set_sender_ip(self, sender='None', ip=Common.myIP + ':10003'):
        global APILIST
        global PEER_IP
        global IS_GROUP
        # Set the current peers ip to the selected ip
        PEER_IP = ip
        """
        If the user has a message icon indicating a message has been received by that person it removes the icon
        on click
        """
        db_mgr.update_alert('0', sender)

        try:
            # Do this to determine if its a group or something else. Not great but it works
            ip.split('.')[2]
            IS_GROUP = False
            # Grab the users api list once
            APILIST = Version1.peerAPI(ip)
        except:
            # If its not an ip address its a group id
            IS_GROUP = True
        raise cherrypy.HTTPRedirect('/client')

    """
    This function is for sending the message to another user.
    Called when the user presses the send button from the client screen
    """

    @cherrypy.tools.json_out()
    @cherrypy.expose()
    def post(self, message, ip=PEER_IP):

        global IS_GROUP
        global PEER_USERNAME
        error = '0'
        # If the message is being sent to a group iterate through the group and send message to each person
        # This is great as the messages send even if the user doesn't support group messages. [Test with Tyrone]
        if IS_GROUP:
            this_group = PEER_IP.split(', ')
            for i in this_group:
                e = ServerSide.send_message(
                    db_mgr.pull_peer_ip_srch(i)[0][0] + ':' + db_mgr.get_port_from_username(i)[0][0], message,
                    cherrypy.session['username'], i, PEER_USERNAME)
                # Check for any error code. Not well implemented yet. For TODO section in report
                if e > error:
                    error = e
            # Check for an errors and report to client
            if error != '0':
                db_mgr.update_table(cherrypy.session['username'],
                                    PEER_IP,
                                    Common.clean(message) + "-[<font color=red>FAILED</font>]",
                                    db_mgr.get_group_id(PEER_IP)[0][0], False)
                raise cherrypy.HTTPRedirect('/client?response=0')
            else:
                db_mgr.update_table(cherrypy.session['username'], PEER_IP,
                                    message, db_mgr.get_group_id(PEER_IP)[0][0])
                raise cherrypy.HTTPRedirect('/client?response=' + str(error))
        # If the message is being sent to individual person just send to their ip
        else:
            error = ServerSide.send_message(ip, message, cherrypy.session['username'],
                                            db_mgr.pull_peer_user_srch(ip.split(':')[0])[0][0])
            # Check for errors and report to client
            if error != '0':
                db_mgr.update_table(cherrypy.session['username'],
                                    db_mgr.pull_peer_user_srch(ip.split(':')[0])[0][0],
                                    Common.clean(message) + "-[<font color=red>FAILED</font>]", 'None', False)
                raise cherrypy.HTTPRedirect('/client?response=' + error)
            elif error == '0':
                db_mgr.update_table(cherrypy.session['username'], db_mgr.pull_peer_user_srch(ip.split(':')[0])[0][0],
                                    message, 'None')
                raise cherrypy.HTTPRedirect('/client?response=' + error)

    """
    This function sets a group from the temporary group list and gets a group id and adds it to the list of groups
    """

    @cherrypy.expose()
    def setGroup(self, list):
        # Remove all the list formatting from the list.
        list = list.replace('(', '')
        list = list.replace('[', '')
        list = list.replace("u'", '')
        list = list.replace(',)', '')
        list = list.replace("'", '')
        list = list.replace("]", '')
        # Get the return message including group id.
        gid = Version1.setGroup(cherrypy.session['username'], cherrypy.session['password'], list)
        # Save the group id and its user list into the database
        gid = gid.replace('\r','')
        gid = gid.replace('\n','')
        db_mgr.add_group_string(str(gid.replace('0, Group Created', '')), list)

        raise cherrypy.HTTPRedirect('/group')

    """
    This adds a group if it exists
    """

    @cherrypy.expose()
    def getGroup(self, group_id):
        page = Version1.getGroup(cherrypy.session['username'], cherrypy.session['password'], group_id)
        if page[0] == '0':
            db_mgr.add_group_string(group_id, str(page.replace('0, Group Found,', '')))
        raise cherrypy.HTTPRedirect('/group')

    """
    The default not found page
    """

    @cherrypy.expose()
    def default(self, *args, **kwargs):
        page = file('index.html', 'r').read()
        page += '<h1 style="color: red">ERROR: 404 Not found</h1>'
        page += file('EndIndex.html', 'r').read()
        return page

    """
    This gets the server Api list
    """

    @cherrypy.expose()
    def apilist(self):
        page = file('index.html', 'r').read()
        page += '<div class="container">'
        iteminput = Version1.listAPI()
        output = iteminput.split('/')
        for i in range(0, len(output)):
            page += "<br>" + unicode(output[i]) + "</br>"
        page += '</div>'
        page += file('EndIndex.html', 'r').read()
        return page

    """
    This reports to the server if other report is not working ~~BACKUP~~
    """

    @cherrypy.expose()
    def report(self):
        page = Version1.report()
        return page

    """
    This function logs the user off
    """

    @cherrypy.expose()
    def log_off(self):
        code = int(Version1.logoff(cherrypy.session['username'], cherrypy.session['password']).split(',')[0])
        Page = file('index.html', 'r').read()
        Page += '<div class="container">'
        Page += '<div class="jumbotron">'
        if code == 0:
            Page += '<h1 align="center">You Logged Off<h1>'
        else:
            Page += '<h1 align="center">Error Logging off<h1>'
        Page += '</div>'
        Page += '</div>'
        Page += file('EndIndex.html', 'r').read()
        # THIS WILL KILL THE REPORTING THREAD - Note may take up to 30s
        Version1.killthread = False
        cherrypy.session['username'] = None
        cherrypy.session['password'] = None
        return Page

    """
    This is the recieve message function. It gets the sent message JSON object and returns an integer.
    """

    @cherrypy.tools.json_in()
    @cherrypy.expose()
    def receiveMessage(self):

        # Get the json data
        data = cherrypy.request.json

        # Get the compulsory fields. If a field is missing return error 1
        try:
            message = data['message']
            sender = data['sender']
            recipient = data['destination']
        except KeyError:
            return '1'
        # Try to get encryption otherwise set encryption to 0
        try:
            encryption = Common.clean(str(data['encryption']))
        except KeyError:
            encryption = '0'

        # Try to get encoding otherwise set encoding to 0
        try:
            encoding = str(Common.clean(data['encoding']))
        except KeyError:
            encoding = '0'

        # Return error if encryption not supported
        if int(encryption) > 3:
            return '9'
        # Try to get group id otherwise set to none
        try:
            groupID = Common.clean(data['groupID'])
        except KeyError:
            groupID = 'None'

        # Try to get hash if not a field except both to 0
        try:
            hash = data['hash']
            hashing = str(data['hashing'])
        except KeyError:
            hash = '0'
            hashing = '0'

        # Check encryption if there is encryption
        if encryption == '3':
            message = EncryptionFunctions.RSA_DCR(message)
            sender = EncryptionFunctions.RSA_DCR(sender)
            recipient = EncryptionFunctions.RSA_DCR(recipient)
            if encoding != '0':
                encoding = EncryptionFunctions.RSA_DCR(encoding)
            if hashing != '0':
                hashing = EncryptionFunctions.RSA_DCR(hashing)
                hash = EncryptionFunctions.RSA_DCR(hash)
            if groupID != 'None':
                groupID = EncryptionFunctions.RSA_DCR(groupID)

        if encryption == '2':
            message = EncryptionFunctions.aes_decrypt(message, 'message')
            sender = EncryptionFunctions.aes_decrypt(sender, 'message')
            recipient = EncryptionFunctions.aes_decrypt(recipient, 'message')
            if encoding != '0':
                encoding = EncryptionFunctions.aes_decrypt(encoding, 'message')
            if hashing != '0':
                hashing = EncryptionFunctions.aes_decrypt(hashing)
                hash = EncryptionFunctions.aes256_enc(hash)
            if groupID != 'None':
                groupID = EncryptionFunctions.aes256_enc(groupID)

        if encryption == '1':
            message = EncryptionFunctions.xor_enc(message)
            sender = EncryptionFunctions.xor_enc(sender)
            recipient = EncryptionFunctions.xor_enc(recipient)
            if encoding != '0':
                encoding = EncryptionFunctions.xor_enc(encoding)
            if hashing != '0':
                hashing = EncryptionFunctions.xor_enc(hashing)
                hash = EncryptionFunctions.xor_enc(hash)
            if groupID != 'None':
                groupID = EncryptionFunctions.xor_enc(groupID)


        # Check if hashing is accepted
        if int(hashing) > 6:
            return '10'
        # Check hashes if there is a hash
        if hashing == '1':
            if EncryptionFunctions.md5(message) != hash:
                return '7'
        elif hashing == '2':
            if EncryptionFunctions.md5_salt(message, sender) != hash:
                return '7'
        elif hashing == '3':
            if EncryptionFunctions.SHA_256(message) != hash:
                return '7'
        elif hashing == '4':
            if EncryptionFunctions.SHA_256_salt(message, sender) != hash:
                return '7'
        elif hashing == '5':
            if EncryptionFunctions.SHA_512(message) != hash:
                return '7'
        elif hashing == '6':
            if EncryptionFunctions.SHA_512_salt(message, sender) != hash:
                return '7'

        # I only accept ascii
        if int(encoding) > 0:
            return '8'

        # Clean all of the items to ensure no html injection/profanity
        message = Common.clean(message)
        sender = Common.clean(sender)
        recipient = Common.clean(recipient)

        # Check that user exists on the server
        # authenticate = False
        # for i in db_mgr.pull_peer_user(None):
        # if sender in i[0]:
        # authenticate = True
        # if not authenticate:
        # return '2'
        # I removed this as sometimes messages won't be received

        # Update the tables
        db_mgr.update_table(sender, recipient, message, groupID)
        db_mgr.update_alert('1', sender)

        return '0'

    """
    This returns a list of my implemented api's
    """

    @cherrypy.expose()
    def listAPI(self):
        page = '\r\n/receiveMessage\r\n/ping [sender]\r\n/receiveMessage [sender] [destination] [message] [groupID] [encoding] [encryption] [hashing] [hash]\r\n/getList [username] [encryption]\r\n/update [username] [location] [ip] [port] [pubKey] [encryption]\r\nEncoding 0\r\nEncryption 0 1 2 3\r\nHashing 0 1 2 3 4 5 6\r\n'
        return page

    """
    Ping, always returns 0 when hit.
    """

    @cherrypy.expose()
    def ping(self, sender=None):
        return '0'

    """
    Sets the page encryption (setter html form submission)
    """

    @cherrypy.expose()
    def setEnc(self, enc):
        Version1.encryption = enc
        raise cherrypy.HTTPRedirect('/login')

    """
    Set the message encryption standard and changes background color based on this
    """

    @cherrypy.expose()
    def setMsgEnc(self, enc):
        global COLOR
        n = 'white'
        if enc == 'XOR':
            n = 'grey'
            ServerSide.encryption = 'XOR'
        if enc == 'RSA':
            n = 'green'
            ServerSide.encryption = 'RSA'
        if enc == 'AES':
            n = 'orange'
            ServerSide.encryption = 'AES'
        if enc == 'None':
            n = 'red'
            ServerSide.encryption = 'None'
        COLOR = n
        raise cherrypy.HTTPRedirect('/client')

    """
    Sets the message hash type
    """

    @cherrypy.expose()
    def setMsgHsh(self, hsh):
        ServerSide.hashing = hsh
        raise cherrypy.HTTPRedirect('/client')

    """
    Gets the pulic key and returns it
    """

    @cherrypy.expose()
    def getPublicKey(self):
        return EncryptionFunctions.PUBLIC_KEY

    """
    This function is responsible for the group conversations interface
    """

    @cherrypy.expose()
    def group(self):
        page = file('index.html', 'r').read()
        page += '<div class="container">'
        page += '   <div class="jumbotron">'
        page += '       <div class="row">'
        page += '           <div class="col-xs-8">'
        page += '               <h1>Group Chat</h1>'
        page += '           </div>'
        page += '       </div>'
        page += '       <div class="row">'
        page += '           <div class="col-xs-8">'
        # Get the list of group menbers
        list = db_mgr.pull_group_members()
        # Display using bootstrap list
        page += '<ul class="list-group">'
        for i in list:
            page += '<li class="list-group-item">' + i[0] + '</li>'
        page += '</ul>'
        # Sets the temporary group list to permanent.
        page += '<a  href="/setGroup?list=' + str(list) + '">submit</a><br>'
        # Sets a new group based on a pre-existing id
        page += '<form action="/getGroup">'
        page += '<input type="text" name="group_id"></input>'
        page += '<button type="submit">Submit</button>'
        page += '</form>'
        # Shows the groups that already exist
        groups = db_mgr.get_all_user_from_group()
        id = db_mgr.get_all_id_from_group()
        page += '<h2> Existing Groups: </h2>'
        for k in range(0, len(id)):
            page += '<ul class="list-group">'
            page += '<a class="list-group-item">' + id[k][0] + '</a>'
            page += '<a class="list-group-item">' + groups[k][0] + '</a>'
            page += '</ul>'
        # Refreshes the divs for the users online
        page += '           </div>'
        page += '           <div class="col-xs-4">'
        page += '               <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>'
        page += '               <script>'
        page += '               function autoRefresh_div(){'
        page += '                   $("#diviplist").load("testtwo?group=group");'
        page += '                   }'
        page += '                   setInterval("autoRefresh_div()", 1000);'
        page += '               </script>'
        page += '               <div id="diviplist"></div>'
        page += '           </div>'
        page += '       </div>'
        page += '   </div>'
        page += '</div>'
        page += file('EndIndex.html', 'r').read()
        return page

    """
    This adds a user to the temporary list
    """

    @cherrypy.expose()
    def addUser(self, username):
        db_mgr.add_group_member(username)
        raise cherrypy.HTTPRedirect('/group')

    """
    This updates a user on the fallback system onto my list
    """

    @cherrypy.tools.json_in()
    @cherrypy.expose()
    def update(self):
        data = cherrypy.request.json

        username = data['username']
        ip = data['ip']
        location = data['location']
        port = data['port']
        pubKey = data['pubKey']

        db_mgr.update_ip('', username, location, ip, port, int(time.time()), pubKey)

        return '0'

    """
    This sends back a user list with all of the online users once fallback system starts
    """

    @cherrypy.expose()
    def getList(self, username, encryption=0):
        database = db_mgr.all_ip()

        list_out = '0,'
        for user in database:
            list_out += '\r\n'
            for data in range(0, len(user)):
                list_out += user[data]
                if data < 5:
                    list_out += ','

        return list_out

    """
    This opens the help documentation. It can be accessed at /help or from the taskbar on any page
    """

    @cherrypy.expose()
    def help(self):
        page = file('index.html', 'r').read()
        page += file('help.html', 'r').read()
        page += file('EndIndex.html', 'r').read()
        return page


"""
The default configuration for the system
"""
if __name__ == '__main__':
    conf = {
        '/': {
            'tools.sessions.on': True,
            'tools.staticdir.root': os.path.abspath(os.getcwd())
        },
        '/static': {
            'tools.staticdir.on': True,
            'tools.staticdir.dir': './public'
        }
    }
    cherrypy.tree.mount(Main(), '/', conf)

    cherrypy.config.update({'server.socket_host': Common.LOCAL_IP,
                            'server.socket_port': Common.LOCAL_PORT,
                            'engine.autoreload.on': False,
                            })

    'Storage should be perminant'
    # cherrypy.engine.subscribe('start', db_mgr.setup_database)
    cherrypy.engine.subscribe('stop', db_mgr.cleanup_database, Version1.logoff(Version1.user, Version1.password))
    cherrypy.engine.start()

    cherrypy.engine.block()