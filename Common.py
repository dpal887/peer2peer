import socket
import urllib2

__author__ = 'ubuntu'

# My local ip and port for cherrypy session
LOCAL_IP = '0.0.0.0'
LOCAL_PORT = 10003

# Names for the databases - TODO: Redundant
DB_STRING = "chat.sqlite"
AD_STRING = "IPs.sqlite"

# Server address - TODO: Make a switch here
SERVER_AD = 'http://andrewchenuoa.pythonanywhere.com'
'''Server to test fallback system'''
# SERVER_AD = 'http://andrewchenuoa.m'

# REF: ninjagecko - http://stackoverflow.com/questions/166506/finding-local-ip-addresses-using-pythons-stdlib
# Gets local ip address
def getNetworkIp():
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    try:
        s.connect(('www.google.co.nz', 0))
    except socket.SO_ERROR:
        s.connect('www.google.com', 0)
    return s.getsockname()[0]


def getPublicIP():
    ip = urllib2.urlopen('http://ip.42.pl/raw').read()
    return ip


# Gets IP address
myIP = getPublicIP()

myPort = 10003

location = '1'

server_key = '491d2a0b5008b43c546df27a'
api_key = '41fb5b5ae4d57c5ee528adb0'

f = open('swearWords.txt')


def profainity(message):
    # TODO: On the uni desktops swap to len(rep)-1]... No idea why...
    rep = f.readline()
    rep = rep.replace('\r', '')
    rep = rep.replace('\n', '')
    msg = message.replace(rep.lower(), rep[0] + "****")
    if (rep == 'wtf'):
        return message
    else:
        return profainity(msg)


def clean(message=None):
    global f
    f = open('swearWords.txt')
    message = profainity(message)
    f.close()
    message = message.replace('<', '&lt')
    message = message.replace('>', '&gt')
    message = message.replace('"', '&quot')
    message = message.replace('\'', '&quot')
    message = message.replace('\\', '&#x2F')
    return message


def Emesg(code):
    if ('10' in code):
        mesg = '10, Hashing standard not supported'
    elif ('11' in code):
        mesg = '11, Connection refused'
    elif ('0' in code):
        mesg = '0, Action was successful'
    elif ('1' in code):
        mesg = '1, Missing Compulsory Field'
    elif ('2' in code):
        mesg = '2, Unauthenticated user'
    elif ('3' in code):
        mesg = '3, Client currently unavailable'
    elif ('4' in code):
        mesg = '4, Database Error'
    elif ('5' in code):
        mesg = '5, Timeout Error'
    elif ('6' in code):
        mesg = '6, Insufficient Permissions'
    elif ('7' in code):
        mesg = '7, Hash does not match'
    elif ('8' in code):
        mesg = '8, Encoding standard not supported'
    elif ('9' in code):
        mesg = '9, Encryption standard not supported'
    else:
        mesg = '12, Substandard Client returned improper error'

    return mesg