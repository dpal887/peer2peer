import time

import ServerSide
import db_mgr


__author__ = 'ubuntu'

import urllib2
import Common
import EncryptionFunctions
import socket

user = ''
password = ''
iplist = ''
request_count = 0
reporting_override = False
length = 0
encryption = 'AES'
killthread = True
wait = True
pingOnly = False

"""
Get the public key of the other person
"""


def getPubKey(ip, username):
    url = ip + "/getPublicKey?sender=" + username
    MsgBak = urllib2.urlopen(url).read()


"""
Get the server api list as accessable in /apilist
"""


def listAPI():
    URL = Common.SERVER_AD + "/listAPI"
    MsgBak = urllib2.urlopen(URL).read()
    return MsgBak


"""
Get a peers api list
"""


def peerAPI(ip):
    URL = 'http://' + ip + "/listAPI"
    try:
        msgbak = urllib2.urlopen(URL, timeout=2).read()
        # For compatibility with up to date version of Ubuntu, split list by \n only. Remove the \r
        msgbak.replace('\r', '')
        msgbak = msgbak.split('\n')
    except urllib2.URLError:
        msgbak = 'No, API'
        msgbak.split(',')
    return msgbak


"""
Calls the url to report to the server.
This is run once every 30-60 seconds and once to authenticate
Hashing is done in function
"""


def report(username, password, location='0'):
    if not reporting_override:
        IP = Common.myIP
        port = str(Common.myPort)
        global encryption
        # Login encryption definition - AES or SHA. AES by default
        if encryption == 'AES':
            username = EncryptionFunctions.aes256_enc(username, 'server')
            password = EncryptionFunctions.aes256_enc(EncryptionFunctions.hash256(password), 'server')
            IP = EncryptionFunctions.aes256_enc(IP, 'server')
            port = EncryptionFunctions.aes256_enc(port, 'server')
            enc = '1'
        elif encryption == 'SHA':
            password = EncryptionFunctions.hash256(password)
            enc = '0'
        else:
            enc = '0'

        URL = Common.SERVER_AD + "/report?username=" + username + "&password=" + password + "&ip=" + IP + "&port=" + port + "&location=" + location + "&enc=" + enc + "&pubkey=" + EncryptionFunctions.aes256_enc(
            EncryptionFunctions.PUBLIC_KEY)
        # High timeout for report - Just in case server being slow
        try:
            MsgBak = urllib2.urlopen(URL, timeout=20).read()
        except urllib2.URLError as e:
            # This means the system is down, implement fallback system
            return Common.Emesg('11')
        except socket.SO_ERROR as e:
            return Common.Emesg('11')
        return str(MsgBak)
    else:
        MsgBak = ServerSide.getList(username)
    return str(MsgBak)


"""
This function logs the user off the system
"""


def logoff(username, password):
    try:
        if not reporting_override:
            hsh_password = EncryptionFunctions.hash256(password)
            URL = Common.SERVER_AD + "/logoff?username=" + username + "&password=" + hsh_password
            MsgBak = urllib2.urlopen(URL).read()
            return str(MsgBak)
        else:
            return 'You cannot logoff now'
    except:
        return 'Failed To Logoff'


"""
This function gets the list of users from the server. It may take a few seconds to update the database
"""


def getList(username, password):
    if not reporting_override:
        hsh_password = EncryptionFunctions.hash256(password)
        URL = Common.SERVER_AD + "/getList?username=" + username + "&password=" + hsh_password
        MsgBak = urllib2.urlopen(URL).read()
        return str(MsgBak)
    else:
        MsgBak = ServerSide.getList(username)
    return str(MsgBak)


"""
This function sets a group in the server
"""


def setGroup(username, password, groupMembers='dpal887,fken007'):
    hsh_password = EncryptionFunctions.hash256(password)
    URL = Common.SERVER_AD + "/setGroup?username=" + username + "&password=" + hsh_password + "&groupMembers=" + groupMembers
    MsgBak = urllib2.urlopen(URL).read()
    return str(MsgBak)


"""
This function gets a group from the server
"""


def getGroup(username, password, groupID='0dfa2bbc7b27'):
    hsh_password = EncryptionFunctions.hash256(password)
    URL = Common.SERVER_AD + "/getGroup?username=" + username + "&password=" + hsh_password + "&groupID=" + groupID
    MsgBak = urllib2.urlopen(URL).read()
    return str(MsgBak)


"""
This function is run as a thread. It is responsible for reporting to the server and getting lists. It is also
responsible for pinging users and updating/getting list in fallback mode. See /help for extra info.
"""


def checkin(location):
    global wait
    while wait:
        time.sleep(1)
    global user
    username = user
    global pingOnly
    if not pingOnly:
        global password
        global killthread
        killthread = True
        while killthread:
            report(username, password, location)
            global iplist
            iplist = getList(username, password).replace('0, Online user list returned', '')
            # TODO: REMOVE THIS!!!
            iplist = getList(username,password).replace(Common.myIP, Common.getNetworkIp())

            # This splits the list of users as it should be split
            iplist = iplist.split('\r\n')
            list = []
            global length

            # This appends each detail in a comma separated list
            for i in range(0, len(iplist)):
                list.append(iplist[i].split(','))

            length = len(list)
            # This loop performs all of the database update functions. Either inserting into the database or simply
            # updating the time
            db_mgr.cleanup_database()
            for i in range(1, length - 1):
                # Update the user list
                if (len(list[i])) == 5:
                    db_mgr.update_ip(str(i), list[i][0], list[i][1], list[i][2], list[i][3], list[i][4], '', )
                elif (len(list[i])) == 6:
                    db_mgr.update_ip(str(i), list[i][0], list[i][1], list[i][2], list[i][3], list[i][4], list[i][5])
            # Runs through and updates the info table with ping data. Ping data returns a numeric based on user online or
            # not
            for j in range(0, len(list) - 2):
                db_mgr.update_info_table(db_mgr.pull_peer_user(None)[j][0], ServerSide.ping(
                    db_mgr.pull_peer_ip(None)[j][0] + ':' + db_mgr.pull_peer_port(None)[j][0], username),
                                         db_mgr.pull_peer_ll(None)[j][0])


            # This part of the thread is only run in fallback mode. Constantly pings users to check if they are online
            # Update all other user's lists
            if reporting_override:
                for j in range(1, len(list) - 1):
                    try:
                        ServerSide.updateList(list[j][2] + ':' + list[j][3], username)
                    except urllib2.URLError:
                        print('User did not support')
                    while True:
                        for k in range(0, len(db_mgr.all_ip())):
                            db_mgr.update_info_table(db_mgr.pull_peer_user(None)[k][0], ServerSide.ping(
                                db_mgr.pull_peer_ip(None)[k][0] + ':' + db_mgr.pull_peer_port(None)[k][0], username),
                                                     db_mgr.pull_peer_ll(None)[k][0])
                        time.sleep(30)
            # When list gets bigger extend the wait time
            # I've done it like this so that the thread only waits a second before recognising flag change
            try:
                for check in range(0, 60 - len(list)):
                    time.sleep(1)
                    print('ok')
                    if not killthread:
                        print('Killed Process')
                        return 0
            except:
                print('Not Ok')
                #List causes wait time now.
                time.sleep(1)

# This runs in fallback mode if the user opts to start the chain - Only pings
    else:
        while True:
            for k in range(0, len(db_mgr.all_ip())):
                db_mgr.update_info_table(db_mgr.pull_peer_user(None)[k][0], ServerSide.ping(
                    db_mgr.pull_peer_ip(None)[k][0] + ':' + db_mgr.pull_peer_port(None)[k][0]),
                                         db_mgr.pull_peer_ll(None)[k][0])

        time.sleep(30)


