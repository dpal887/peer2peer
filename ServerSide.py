import EncryptionFunctions
import db_mgr

__author__ = 'ubuntu'

import Common
import urllib2
import json
import socket

encryption = 'None'
hashing = 'No'
fallback_address = ''

"""
This function sends a message to another user.
It takes a number of arguments based on my comparability.
"""


def send_message(dest, message, sender, destination, groupID=''):
    # Set the hashing standard
    if hashing == 'No':
        hNum = '0'
        hMesg = ''
    elif hashing == 'md5':
        hNum = '1'
        hMesg = EncryptionFunctions.md5(message)
    elif hashing == 'md5-salt':
        hNum = '2'
        hMesg = EncryptionFunctions.md5_salt(message, sender)
    elif hashing == 'sha256':
        hNum = '3'
        hMesg = EncryptionFunctions.SHA_256(message)
    elif hashing == 'sha256-salt':
        hNum = '4'
        hMesg = EncryptionFunctions.SHA_256_salt(message, sender)
    elif hashing == 'sha512':
        hNum = '5'
        hMesg = EncryptionFunctions.SHA_512(message)
    elif hashing == 'sha512-salt':
        hNum = '6'
        hMesg = EncryptionFunctions.SHA_512_salt(message, sender)
    else:
        hNum = '0'
        hMesg = ''

    url = "http://" + dest + "/receiveMessage"
    # Obtain public key
    pubkey = db_mgr.pull_peer_key(destination)

    # If the public key is invalid get one to prevent breaking the system
    if len(pubkey) < 5:
        pubkey = EncryptionFunctions.PUBLIC_KEY

    # Set the encryption standard
    if encryption == 'XOR':
        enc_type = '1'
        message = EncryptionFunctions.xor_enc(message)
        sender = EncryptionFunctions.xor_enc(sender)
        destination = EncryptionFunctions.xor_enc(destination)
        hNum = EncryptionFunctions.xor_enc(hNum)
        hMesg= EncryptionFunctions.xor_enc(hMesg)
        groupID = EncryptionFunctions.xor_enc(groupID)
    elif encryption == 'AES':
        enc_type = '2'
        message = EncryptionFunctions.aes256_enc(message, 'message')
        sender = EncryptionFunctions.aes256_enc(sender, 'message')
        destination = EncryptionFunctions.aes256_enc(destination, 'message')
        hNum = EncryptionFunctions.aes256_enc(hNum)
        hMesg = EncryptionFunctions.aes256_enc(hMesg)
        groupID = EncryptionFunctions.aes256_enc(groupID)
    elif encryption == 'RSA':
        enc_type = '3'
        message = EncryptionFunctions.RSA_ENCR(pubkey, message)
        sender = EncryptionFunctions.RSA_ENCR(pubkey, sender)
        destination = EncryptionFunctions.RSA_ENCR(pubkey, destination)
        hNum = EncryptionFunctions.RSA_ENCR(pubkey, hNum)
        hMesg = EncryptionFunctions.RSA_ENCR(pubkey,hMesg)
        groupID = EncryptionFunctions.RSA_ENCR(pubkey, groupID)

    else:
        enc_type = '0'
    # Put the arguments into JSON formatting
    try:
        data = json.dumps(
            dict(message=message, sender=sender, destination=destination, encryption=enc_type, groupID=groupID, hashing=hNum,
                 hash=hMesg))
        # Create the request
        req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
        # Send the request
        f = urllib2.urlopen(req, timeout=4)
        # Get the response
        response = f.read()
        f.close()
        return response
    # Return errors if the url open failed
    except urllib2.URLError as e:
        return '11'
    except socket.timeout, e:
        return '11'


"""
This function tests if a user is online by pinging them and hopefully recieving a 0 back
"""


def ping(dest, username):
    url = "http://" + dest + "/ping?sender=" + username
    try:
        response = urllib2.urlopen(url, timeout=1).read()
        return '0'
    except urllib2.URLError:
        return '2'
    except socket.timeout, e:
        return '2'
    except:
        return '2'


"""
This function gets a list of online users on the fallback system from another peer who has implemented this system
"""


def getList(username, enc='0'):
    try:
        global fallback_address
        url = "http://" + fallback_address + "/getList?username=" + username + "&encryption=" + enc
        response = urllib2.urlopen(url).read()
        return response
    except urllib2.URLError:
        return ''


"""
This function updates all users on the list with my details for use in the fallback system. More can be found at
/help
"""


def updateList(ip, username):
    ip = ip.replace(' ', '')
    url = "http://" + ip + "/update"
    # Sending object as JSON
    data = json.dumps(dict(username=username, ip=Common.myIP, location=Common.location, port=Common.LOCAL_PORT,
                           pubkey=EncryptionFunctions.PUBLIC_KEY))
    req = urllib2.Request(url, data, {'Content-Type': 'application/json'})
    try:
        obj = urllib2.urlopen(req, timeout=5)
        msg = obj.read()
        obj.close()
    except urllib2.URLError:
        obj = ''
    return msg