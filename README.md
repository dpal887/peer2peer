__AUTHOR DANIEL PALEY__
#py2P chat client

To run the program please navigate to this directory and run

From linux command line:
python Chat.py

From python command line:
execfile('Chat.py')

Dependencies:

All dependencies come installed with python except possibly:

cherrypy

pyCrypto

To install these packages ensure you have pip: sudo apt-get install python-pip

pip install cherrypy

pip install pyCrypto

You will need to disable the server or change the server link in Common.py and try to log in (with all fields filled out)
in order to test fallback.

Once you have lanched the program navigate to 0.0.0.0:10003/ in your browser or 0.0.0.0:10003/help if you need help.

For testing all features except fallback system test with: tshe835
For testing fallback system test with: bdre332

** A MORE DETAILED HELP MENU CAN BE FOUND AT 0.0.0.0:10003/help (with the program running) **

PLEASE NOTE:Sometimes the user list takes about 30 seconds to show on the screen. This is due to multiple threads
accessing the same database causing a lock-out error. In very rare cases the program may need to be re-run and
the list may not show however I think I have solved this issue.

